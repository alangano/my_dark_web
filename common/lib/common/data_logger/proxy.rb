
module DataLogger

class Proxy
   def initialize(i_obj,i_data)
      @obj = i_obj
      @data = i_data
   end
   def method_missing(i_method,i_msg,i_auxdata=nil,&i_callback)
      if level = levels[i_method]
         t_context =
            @obj.instance_variable_defined?('@logger_obj_context') ?
               @obj.instance_variable_get('@logger_obj_context') : nil
               
         t_context.tap { |oc|
            oc and
               @data[:obj_context] =
                  @obj.instance_variable_get('@logger_obj_context')
         }
         @data[:level] = i_method
         @data[:lvl] = level[:pre]
         @data[:msg] = i_msg
         i_auxdata and @data[:auxdata] = i_auxdata
         handlers.each { |h| h.emit(@data) }
      else
         raise "missing log-level: #{i_method}"
      end
      nil
   end
   def obj_context=(i_context)
      @obj.instance_variable_set('@logger_obj_context',i_context)
   end
   def levels
      DataLogger::Logger.instance.levels
   end
   def handlers
      DataLogger::Logger.instance.handlers
   end
end

end # module DataLogger

