
require 'singleton'

require_relative 'proxy'
require_relative 'handler/null'
require_relative 'handler/stderr'
require_relative 'handler/json_file'

module DataLogger

class Logger
   include Singleton
   attr_accessor :levels
   attr_accessor :handlers
   def initialize
      self.levels = []
      self.handlers = []

      self.levels =
      {
         debug3:  { pre: '3' },
         debug2:  { pre: '2' },
         debug1:  { pre: '1' },
         debug:   { pre: 'D' },
         info:    { pre: 'I' },
         warn:    { pre: 'W' },
         error:   { pre: 'E' },
         fatal:   { pre: 'F' },
         sql:     { pre: 'S' },
      }

      self.handlers << Handler::STDERR.new(:info)
   end
end

end # module DataLogger


module Kernel
   def logger
      cl = caller_locations(1).first
      DataLogger::Proxy.new(
         self,
         timestamp: Time.now.strftime('%Y-%m-%d %H:%M:%S.%9N %Z'),
         class: self.class.to_s,
         method: cl.base_label,
         file: cl.absolute_path,
         line: cl.lineno,
         objid: self.__id__,
         threadid: Thread.current.__id__,
      )
   end
end



