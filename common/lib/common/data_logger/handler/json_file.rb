
require 'json'

module DataLogger
module Handler

class JSONFile
   def initialize(i_name)
      @name = i_name
      @prev_file = nil
      @fh = nil
   end
   def emit(d)
      if file != @prev_file
         if @fh
            @fh.puts "/*closing*/"
            @fh.close
         end
         @fh = file.open('a+')
         @fh.puts "/*opening - #{Time.now.strftime('%Y-%m-%d %H:%M:%S')}*/"
         @prev_file = file
      end
      @fh.puts JSON.generate(d)
   end
   def file
      log_dir / "#{@name}.jsonlog"
   end
   def log_dir
      Pathname.new("#{APP_ROOT}/log")
   end
end

end # module Handler
end # module DataLogger


