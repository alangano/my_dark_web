
module DataLogger
module Handler

class STDERR
   attr_accessor :log_level
   def initialize(i_log_level=:debug)
      @log_level = i_log_level
   end
   def emit(d)
      translate_level(d[:level]) >= translate_level(@log_level.to_sym) and
         ::STDERR.puts format(d)
   end
   def translate_level(i_level)
      levels.keys.index(i_level)
   end
   def levels
      DataLogger::Logger.instance.levels
   end
   def format(d)
      [
         d[:lvl],
         d[:timestamp],
         #"[#{d[:class]}::#{d[:method]}]",
         "[#{File.basename(d[:file],'.rb')}:#{d[:method]}]",
         #d[:obj_context] ? "[#{d[:obj_context]}]" : '',
         d[:msg],
      ].join(' ')
   end
end

end # module Handler
end # module DataLogger

