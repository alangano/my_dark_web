
module VersionedClass
   def self.included(i_class)
      i_class.extend ClassMethods
   end
   module ClassMethods
      def new(*i_args)
         t_version =
            self.constants.
               find_all { |version| version =~ /\AV\d+\Z/ }.
               collect { |version| version.to_s.sub(/\AV/,'').to_i }.
               max.
               yield_self { |max_version| "V#{max_version}".to_sym }

         self.const_get(t_version).new(*i_args)
      end
      def new_at_version(i_version,*i_args)
         t_version =
            self.constants.
               find_all { |version| version =~ /\AV\d+\Z/ }.
               collect { |version| version.to_s.sub(/\AV/,'').to_i }.
               find { |version| version == i_version }.
               tap { |version|
                  version.nil? and raise "#{name}::V#{i_version} does not exist"
               }.
               yield_self { |version| "V#{version}".to_sym }

         self.const_get(t_version).new(*i_args)
      end
   end
end

