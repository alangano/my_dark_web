
require 'rbnacl'

module MyDarkWeb
module Common
module Crypto
module V1

class KeyPair
   include CryptoError
   include Helpers
   attr_reader :private_key
   def initialize
      @private_key = nil
   end
   def create
      @private_key = RbNaCl::PrivateKey.generate
      self
   end
   def serialize
      make_json(serialize_to_h)
   end
   def serialize_to_h
      {
         schema:        schema('KEYPAIR'),
         keypair:       binencode(@private_key.to_bytes)
      }
   end
   def deserialize(i_json)
      t_data = parse_json(i_json)
      t_data.fetch(:schema) == schema('KEYPAIR') or err 'schema mismatch'
      @private_key = RbNaCl::PrivateKey.new(bindecode(t_data.fetch(:keypair)))
      self
   end
   def pubkey
      V1::PubKey.new(@private_key.public_key)
   end
   def decrypt(i_json,i_pubkey)
      t_data = parse_json(i_json)
      t_data.fetch(:schema) == ['V1','PubKey','CIPHER'] or err 'schema mismatch'

      t_box = RbNaCl::Box.new(i_pubkey.pubkey,@private_key)

      t_box.decrypt(
         bindecode(t_data.fetch(:nonce)),
         bindecode(t_data.fetch(:cipher))
      )
   end
   def signature(i_data)
      make_json(signature_to_h(i_data))
   end
   def signature_to_h(i_data)
      t_signing_key = RbNaCl::SigningKey.generate
      {
         schema:     schema('SIGNATURE'),
         verify_key: binencode(t_signing_key.verify_key.to_s),
         signature:  binencode(t_signing_key.sign(i_data))
      }
   end
end

end # module V1
end # module Crypto
end # module Common
end # module MyDarkWeb

