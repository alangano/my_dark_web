
require 'rbnacl'

module MyDarkWeb
module Common
module Crypto
module V1

class SigningKey
   include CryptoError
   include Helpers
   attr_reader :signing_key
   def initialize
      @signing_key = nil
   end
   def create
      @signing_key = RbNaCl::SigningKey.generate
      self
   end
   def serialize
      make_json(serialize_to_h)
   end
   def serialize_to_h
      {
         schema:        schema('SIGNINGKEY'),
         signing_key:   binencode(@signing_key.to_bytes)
      }
   end
   def deserialize(i_json)
      t_data = parse_json(i_json)
      t_data.fetch(:schema) == schema('SIGNINGKEY') or err 'schema mismatch'
      @signing_key =
         RbNaCl::PrivateKey.new(bindecode(t_data.fetch(:signing_key)))
      self
   end
   def verify_key
      V1::VerifyKey.new(@signing_key.verify_key)
   end
   def decrypt(i_json,i_pubkey)
      t_data = parse_json(i_json)
      t_data.fetch(:schema) == ['V1','PubKey','CIPHER'] or err 'schema mismatch'

      t_box = RbNaCl::Box.new(i_pubkey.pubkey,@signing_key)

      t_box.decrypt(
         bindecode(t_data.fetch(:nonce)),
         bindecode(t_data.fetch(:cipher))
      )
   end
   def signature(i_data)
      make_json(signature_to_h(i_data))
   end
   def signature_to_h(i_data)
      t_signing_key = RbNaCl::SigningKey.generate
      {
         schema:     schema('SIGNATURE'),
         verify_key: binencode(t_signing_key.verify_key.to_s),
         signature:  binencode(t_signing_key.sign(i_data))
      }
   end
end

end # module V1
end # module Crypto
end # module Common
end # module MyDarkWeb

