
require 'ostruct'

module MyDarkWeb
module Common
module Crypto
module PEM

def self.serialize(i_data)
   i_data[:crypto_version] = 1
   d = OpenStruct.new(i_data)
   [
      "src_application: common/crypto",
      "crypto_version: #{Crypto::VERSION}",
      #"generated: #{Time.now.utc.strftime('%Y-%m-%d %H:%M:%S.%3N')}",
      (d.to_h.
         find_all { |n,v| !([:name,:crypto_version,:data].index(n)) }.
         collect { |n,v| "#{n}: #{v}" }),
      '',
      "#{'-'*5}BEGIN #{d.name.upcase}#{'-'*5}",
      Base64.encode64(d.data).lines.collect { |line| line.chomp },
      "#{'-'*5}END #{d.name.upcase}#{'-'*5}",
   ].flatten.join("\n")
end
def self.streaming_serialize(i_data)
   i_data[:crypto_version] = Crypto::VERSION
   d = OpenStruct.new(i_data)
   yield "src_application: common/crypto"
   yield "crypto_version: #{Crypto::VERSION}"
   d.to_h.
      find_all { |n,v| !([:name,:crypto_version,:data].index(n)) }.
      each { |n,v|
         yield "#{n}: #{v}"
      }
   yield ''

   yield "#{'-'*5}BEGIN #{d.name.upcase}#{'-'*5}"
   Base64.streaming_enum_encode64(d.data).each { |line|
      yield line
   }
   yield "#{'-'*5}END #{d.name.upcase}#{'-'*5}"
   nil
end
def self.deserialize(i_pem)
   t_out = OpenStruct.new({
      name: nil,
      data: nil,
   })
   t_data = []
   t_data_section = false
   i_pem.lines.
      collect { |line| line.chomp }.
      each { |line|
         if t_data_section
            if line =~ /\A-----END (.*)-----\Z/
               t_out.name == $1 or
                  raise CryptoError.err "parse of data section failed"
               t_data_section = false
            else
               t_data << line
            end
         else
            if line =~ /\A([a-z_0-9]+): (.*)\Z/
               n,v = $1,$2
               t_out.__send__("#{n}=",v)
            elsif line =~ /\A\Z/
            elsif line =~ /\A-----BEGIN (.*)-----\Z/
               t_out.name = $1
               t_data_section = true
            elsif line =~ /\A-----END (.*)-----\Z/
               raise CryptoError.err "end data section wrongly encountered"
            else
               raise CryptoError.err "parsing error"
            end
         end
      }

   t_data_section and raise Crypto.err "end of data section not reached"
   t_out.data = Base64.decode64(t_data.join("\n"))

   t_out.crypto_version.to_i == Crypto::VERSION or
      Crypto.err "wrong class version : #{t_data.code_version}"

   t_out
end
def self.streaming_deserialize(i_enum)
   t_out = OpenStruct.new({
      name: nil,
      data: nil,
   })
   t_data_section = false
   i_enum.each { |line|
      line.chomp!
      if t_data_section
         if line =~ /\A-----END (.*)-----\Z/
            t_out.name == $1 or
               raise CryptoError.err "parse of data section failed"
            yield(:last)
            t_data_section = false
         else
            yield(:data,Base64.decode64(line))
         end
      else
         if line =~ /\A([a-z_0-9]+): (.*)\Z/
            n,v = $1,$2
            t_out.__send__("#{n}=",v)
         elsif line =~ /\A\Z/
         elsif line =~ /\A-----BEGIN (.*)-----\Z/
            t_out.name = $1
            yield(:info,t_out)
            t_data_section = true
         elsif line =~ /\A-----END (.*)-----\Z/
            raise CryptoError.err "end data section wrongly encountered"
         else
            raise CryptoError.err "parsing error"
         end
      end
   }

   nil
end

end # module PEM
end # module Crypto
end # module Common
end # module MyDarkWeb


