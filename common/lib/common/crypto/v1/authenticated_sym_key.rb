
require 'rbnacl'

module MyDarkWeb
module Common
module Crypto
module V1

class AuthenticatedSymKey
   include CryptoError
   include Helpers
   attr_reader :key
   def initialize
      @box  = nil
      @key  = nil
   end
   def create
      @key = RbNaCl::Random.random_bytes(RbNaCl::SecretBox.key_bytes)
      @box = RbNaCl::SecretBox.new(@key)
      self
   end
   def _create_empty(i_cipher_name=cipher_name)
      @cipher = OpenSSL::Cipher.new(i_cipher_name)
      @decipher = OpenSSL::Cipher.new(i_cipher_name)
      @cipher.encrypt
      @decipher.decrypt

      self
   end
   def set_key(i_key)
      @key = i_key
      @box = RbNaCl::SecretBox.new(@key)
      self
   end
   def key_length
      @cipher.key_len
   end
   def encrypt(i_data)
      make_json(encrypt_to_h(i_data))
   end
   def encrypt_to_h(i_data)
      t_nonce = RbNaCl::Random.random_bytes(@box.nonce_bytes)
      {
         schema:     schema('CIPHER'),
         nonce:      binencode(t_nonce),
         cipher:
            binencode(
               @box.encrypt(
                  t_nonce,
                  i_data.nil? ? 'NULL:' : "DATA:#{i_data}"
               )
            )
      }
   end
   def decrypt(i_json)
      decrypt_from_h(parse_json(i_json))
   end
   def decrypt_from_h(i_data)
      i_data.fetch(:schema) == schema('CIPHER') or err 'schema mismatch'

      @box.decrypt(
         bindecode(i_data.fetch(:nonce)),
         bindecode(i_data.fetch(:cipher))
      ).yield_self { |tagged_data|
         t_tag = tagged_data.sub(/:.*$/,'')
         t_tag == 'NULL' ?  nil : tagged_data.sub(/\ADATA:/,'')
      }
   end
   def serialize
      make_json(to_h)
   end
   def to_h
      @key.nil? and Crypto.err "the key is not in program-space"

      {
         schema:  schema('AUTHENTICATEDSYMKEY'),
         key:     binencode(@key)
      }
   end
   def deserialize(i_json)
      t_data = parse_json(i_json)
      t_data.fetch(:schema) == schema('AUTHENTICATEDSYMKEY') or
         err 'schema mismatch'

      @key = bindecode(t_data.fetch(:key))
      @box = RbNaCl::SecretBox.new(@key)

      self
   end
   private
   def clear_key
      @key.gsub!(/./,'x') if !@key.nil?  # attempt to wipe memory location
      @key = nil
      self
   end
end

end # module V1
end # module Crypto
end # module Common
end # module MyDarkWeb

