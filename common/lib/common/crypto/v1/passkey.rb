
require 'rbnacl'

module MyDarkWeb
module Common
module Crypto
module V1

class PassKey
   include CryptoError
   include Helpers
   def initialize
      @salt          = nil
      @ops_limit     = nil
      @mem_limit     = nil
      @auth_sym_key  = nil
   end
   def create(i_ops_limit=5,i_mem_limit=7256678)
      @salt =
         RbNaCl::Random.random_bytes(RbNaCl::PasswordHash::Argon2::SALTBYTES)
      @ops_limit = i_ops_limit
      @mem_limit = i_mem_limit
      self
   end
   def set_passphrase(i_passphrase)
      @salt.nil? and err "create() has not been run"

      @auth_sym_key = AuthenticatedSymKey.new.create
      @auth_sym_key.set_key(
         RbNaCl::PasswordHash.argon2(
            i_passphrase, 
            @salt,
            @ops_limit,
            @mem_limit,
            RbNaCl::SecretBox.key_bytes
         )
      )

      self
   end
   def passphrase_set?
      !@auth_sym_key.nil?
   end
   def serialize
      make_json(serialize_to_h)
   end
   def serialize_to_h
      {
         schema:        schema('PASSKEY'),
         salt:          binencode(@salt),
         ops_limit:     @ops_limit,
         mem_limit:     @mem_limit
      }
   end
   def deserialize(i_json)
      t_data = parse_json(i_json)
      t_data.fetch(:schema) == schema('PASSKEY') or err 'schema mismatch'
      load_from_h(t_data)
      self
   end
   def load_from_h(i_hash)
      @salt       = bindecode(i_hash.fetch(:salt))
      @ops_limit  = i_hash.fetch(:ops_limit)
      @mem_limit  = i_hash.fetch(:mem_limit)
      self
   end
   def encrypt(i_data)
      @auth_sym_key.encrypt(i_data)
   end
   def encrypt_to_h(i_data)
      @auth_sym_key.encrypt_to_h(i_data)
   end
   def decrypt(i_edata)
      @auth_sym_key.nil? and err 'the passphrase is not set'
      @auth_sym_key.decrypt(i_edata)
   end
   def decrypt_from_h(i_edata)
      @auth_sym_key.nil? and err 'the passphrase is not set'
      @auth_sym_key.decrypt_from_h(i_edata)
   end
   private
end

end # module V1
end # module Crypto
end # module Common
end # module MyDarkWeb

