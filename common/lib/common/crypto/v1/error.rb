
module MyDarkWeb
module Common
module Crypto

class Error < Exception
end

module CryptoError
   def err(i_msg)
      raise Error.new(i_msg)
   end
   def self.err(i_msg)
      raise Error.new(i_msg)
   end
end

end # module Crypto
end # module Common
end # module MyDarkWeb


