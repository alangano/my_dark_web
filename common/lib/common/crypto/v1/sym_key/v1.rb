
require 'openssl'

module MyDarkWeb
module Common
module Crypto

class SymKey
   include VersionedClass
end

class SymKey::V1
   CIPHER_NAME='aes-256-cbc'
   include CryptoError
   include Helpers
   #attr_reader :key
   def initialize
      @cipher     = nil
      @decipher   = nil
      @key        = nil
   end
   def create(i_cipher_name=CIPHER_NAME)
      @cipher = OpenSSL::Cipher.new(i_cipher_name)
      @decipher = OpenSSL::Cipher.new(i_cipher_name)

      @cipher.encrypt
      @decipher.decrypt

      @key = OpenSSL::Random.random_bytes(@cipher.key_len)
      @cipher.key = @key
      @decipher.key = @key

      self
   end
   def create_empty(i_cipher_name=CIPHER_NAME)
      @cipher = OpenSSL::Cipher.new(i_cipher_name)
      @decipher = OpenSSL::Cipher.new(i_cipher_name)

      @cipher.encrypt
      @decipher.decrypt

      self
   end
   def set_key(i_key)
      @key           = i_key
      @cipher.key    = @key
      @decipher.key  = @key
      self
   end
   def key_length
      @cipher.key_len
   end
   def encrypt(i_data)
      make_json(encrypt_to_h(i_data))
   end
   def encrypt_to_h(i_data)
      t_iv = OpenSSL::Random.random_bytes(@cipher.iv_len)
      @cipher.iv = t_iv

      t_tagged_data = i_data.nil? ? 'NULL:' : "DATA:#{i_data}"

      t_edata = @cipher.update(t_tagged_data)
      t_edata << @cipher.final

      {
         scheme:     scheme('CIPHER'),
         iv:         binencode(t_iv),
         data:       binencode(t_edata)
      }
   end
   def decrypt(i_json)
      t_data = parse_json(i_json)
      t_data.fetch(:scheme) == scheme('CIPHER') or err 'scheme mismatch'

      @decipher.iv  = bindecode(t_data.fetch(:iv))
      t_tagged_data = @decipher.update(bindecode(t_data.fetch(:data)))
      t_tagged_data << @decipher.final

      t_tag = t_tagged_data.sub(/:.*$/,'')
      t_tag == 'NULL' ?  nil : t_tagged_data.sub(/\ADATA:/,'')
   end
   def serialize
      make_json(serialize_to_h)
   end
   def serialize_to_h
      @key.nil? and Crypto.err "the key is not in program-space"

      {
         scheme:        scheme('SYMKEY'),
         cipher_name:   @cipher.name,
         key:           binencode(@key)
      }
   end
   def deserialize(i_json)
      t_data = parse_json(i_json)
      t_data.fetch(:scheme) == scheme('SYMKEY') or err 'scheme mismatch'

      @key = bindecode(t_data.fetch(:key))

      @cipher = OpenSSL::Cipher.new(t_data.fetch(:cipher_name))
      @cipher.encrypt
      @cipher.key = @key

      @decipher = OpenSSL::Cipher.new(t_data.fetch(:cipher_name))
      @decipher.decrypt
      @decipher.key = @key

      self
   end
   private
   def clear_key
      @key.gsub!(/./,'x') if !@key.nil?  # attempt to wipe memory location
      @key = nil
      self
   end
end

end # module Crypto
end # module Common
end # module MyDarkWeb


