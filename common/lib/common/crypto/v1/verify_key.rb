
require 'rbnacl'

module MyDarkWeb
module Common
module Crypto
module V1

class VerifyKey
   include CryptoError
   include Helpers
   attr_reader :verify_key
   def initialize(i_verify_key_ser)
      t_data = parse_json(i_verify_key_ser)
      t_data.fetch(:schema) == ['V1','SigningKey','SIGNATURE'] or
         err 'schema mismatch'
      @verify_key = RbNaCl::VerifyKey.new(bindecode(t_data.fetch(:verify_key)))
      @signature = bindecode(t_data.fetch(:signature))
   end
   def verified?(i_data)
      @verify_key.verify(@signature,i_data)
   rescue RbNaCl::BadSignatureError
      false
   end
end

end # module V1
end # module Crypto
end # module Common
end # module MyDarkWeb

