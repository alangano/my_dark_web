
require 'rbnacl'

module MyDarkWeb
module Common
module Crypto
module V1

class PubKey
   include CryptoError
   include Helpers
   attr_reader :pubkey
   def initialize(i_pubkey=nil)
      @pubkey = i_pubkey
   end
   def hex
      @pubkey.to_bytes.unpack('H*').first
   end
   def serialize
      make_json(serialize_to_h)
   end
   def serialize_flat
      make_json_flat(serialize_to_h)
   end
   def to_b64
      binencode(@pubkey.to_bytes)
   end
   def serialize_to_h
      {
         schema:        schema('PUBKEY'),
         pubkey:        binencode(@pubkey.to_bytes)
      }
   end
   def deserialize(i_json)
      t_data = parse_json(i_json)
      t_data.fetch(:schema) == schema('PUBKEY') or err 'schema mismatch'
      @pubkey = RbNaCl::PublicKey.new(bindecode(t_data.fetch(:pubkey)))
      self
   end
   def encrypt(i_data,i_keypair)
      make_json(encrypt_to_h(i_data,i_keypair))
   end
   def encrypt_to_h(i_data,i_keypair)
      t_box = RbNaCl::Box.new(@pubkey,i_keypair.private_key)
      t_nonce = RbNaCl::Random.random_bytes(t_box.nonce_bytes)

      {
         schema:  schema('CIPHER'),
         nonce:   binencode(t_nonce),
         cipher:  binencode(t_box.encrypt(t_nonce,i_data))
      }
   end
   def _verify_signature(i_json,i_data)
      t_data = parse_json(i_json)
      t_data.fetch(:schema) == ['V1','KeyPair''SIGNATURE'] or
         err 'schema mismatch'

      @pubkey.verify(
         OpenSSL::Digest::SHA512.new,
         bindecode(t_data.fetch(:signature)),
         i_data
      )
   end
   def fingerprint
      OpenSSL::Digest::SHA1.
         hexdigest(@pubkey.to_bytes).
         scan(/../).
         join(':').
         upcase
   end
end

end # module V1
end # module Crypto
end # module Common
end # module MyDarkWeb

