
require 'base64'

module MyDarkWeb
module Common
module Crypto

module Helpers
   def schema(i_action)
      self.class.to_s.split('::')[-2..-1] + [i_action]
   end
   def binencode(i_text)
      Base64.strict_encode64(i_text)
   end
   def bindecode(i_text)
      Base64.strict_decode64(i_text)
   end
   def make_json(i_obj)
      JSON.pretty_generate(i_obj)
   end
   def make_json_flat(i_obj)
      JSON.generate(i_obj)
   end
   def parse_json(i_json)
      JSON.parse(i_json,symbolize_names:true)
   end
end # module Helpers

end # module Crypto
end # module Common
end # end # module MyDarkWeb

