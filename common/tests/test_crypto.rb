#!/bin/env ruby
#!/bin/env jruby
$VERBOSE = true

#require 'securerandom'

require 'pathname'
APP_ROOT ||= Pathname.new(ENV['APP_ROOT'])
$LOAD_PATH.unshift("#{APP_ROOT}/common/lib")

#require 'global'
require 'common/crypto'

require 'test/unit'

class Test_Crypto < Test::Unit::TestCase
   Crypto = MyDarkWeb::Common::Crypto
   def setup
   end
   def teardown
   end
   def test_authenticated_symkey
      t_symkey = Crypto::AuthenticatedSymKey.new.create
      'this is a test message'.tap { |m|
         t_edata = t_symkey.encrypt(m)
         assert_equal m, t_symkey.decrypt(t_edata)
      }

      t_symkey_ser = t_symkey.serialize

      t_symkey2 = Crypto::AuthenticatedSymKey.new.deserialize(t_symkey_ser)

      assert_equal(
         t_symkey.instance_variable_get('@key'),
         t_symkey2.instance_variable_get('@key')
      )

      'this is another test message'.tap { |m|
         t_symkey = Crypto::AuthenticatedSymKey.new.deserialize(t_symkey_ser)
         t_edata = t_symkey.encrypt(m)
         assert_equal m, t_symkey2.decrypt(t_edata)
      }
   end
   def test_passkey
      t_passkey = Crypto::PassKey.new.create

      assert !t_passkey.passphrase_set?
      t_passkey.set_passphrase('ack')
      assert t_passkey.passphrase_set?

      t_passkey_ser = t_passkey.serialize
      t_passkey2 = Crypto::PassKey.new.deserialize(t_passkey_ser)
      t_passkey2.set_passphrase('ack')

      'whoop dee doo'.tap { |m|
         t_emsg = t_passkey.encrypt(m)
         assert_equal m, t_passkey.decrypt(t_emsg)
         assert_equal m, t_passkey2.decrypt(t_emsg)
      }
   end
   def test_keypair
      t_keypair = Crypto::KeyPair.new.create
      t_keypair_ser = t_keypair.serialize
      t_keypair2 = Crypto::KeyPair.new.deserialize(t_keypair_ser)

      t_pubkey = t_keypair.pubkey
      t_pubkey_ser = t_pubkey.serialize
      t_pubkey2 = Crypto::PubKey.new.deserialize(t_pubkey_ser)

      'whack a doo dee'.tap { |m|
         t_emsg = t_pubkey.encrypt(m,t_keypair)
         assert_equal m,t_keypair.decrypt(t_emsg,t_pubkey)
      }

      'doh ree may yo!'.tap { |m|
         t_emsg = t_pubkey2.encrypt(m,t_keypair2)
         assert_equal m,t_keypair2.decrypt(t_emsg,t_pubkey2)
      }
   end
   def test_signatures
      t_signing_key = Crypto::SigningKey.new.create

      'the package is secured'.tap { |m|
         t_sig_ser = t_signing_key.signature(m)
         t_verify_key = Crypto::VerifyKey.new(t_sig_ser)
         assert t_verify_key.verified?(m)
         assert !t_verify_key.verified?('derp!')
      }

      t_signing_key_ser = t_signing_key.serialize
      t_signing_key2 = Crypto::SigningKey.new.deserialize(t_signing_key_ser)
      'the package is secured'.tap { |m|
         t_sig_ser = t_signing_key2.signature(m)
         t_verify_key = Crypto::VerifyKey.new(t_sig_ser)
         assert t_verify_key.verified?(m)
      }
   end
   def _test_keyring
      t_keyring = Crypto::KeyRing.new

#      t_keypack = t_keyring.create('keyring passphrase',auth,1024)
#      t_keyring.authenticate('keyring passphrase',auth,t_keypack)
#
#      t_keyring = Crypto::KeyRing.new
#      assert !t_keyring.authenticated?
#      t_keyring.authenticate('keyring passphrase',auth,t_keypack)
#      assert t_keyring.authenticated?
#
#      t_keyring = Crypto::KeyRing.new
#      assert_raises(Crypto::Error) {
#         t_keyring.authenticate('bad password',auth,t_keypack)
#      }
#      assert !t_keyring.authenticated?
#
#      begin
#         t_keyring.authenticate('bad password',auth,t_keypack)
#      rescue Crypto::Error => e
#         assert_equal 'failed to unlock master key',e.message
#      end
#      assert !t_keyring.authenticated?
#
#      t_keyring.authenticate('keyring passphrase',auth,t_keypack)
#      assert t_keyring.authenticated?
   end
   def _test_change_passphrase
      t_keyring = Crypto::KeyRing.new
      t_keypack = t_keyring.create('password1',auth,1024)

      t_keyring = Crypto::KeyRing.new
      t_keyring.authenticate('password1',auth,t_keypack)
      t_keyring.change_passphrase('password1','password2',auth) { |a,b|
         t_keypack[:passkey] = a
         t_keypack[:enc_mkey] = b
      }

      t_keyring2 = Crypto::KeyRing.new
      assert_raises(Crypto::Error) {
         t_keyring2.authenticate('password1',auth,t_keypack)
      }
      t_keyring2.authenticate('password2',auth,t_keypack)
   end
   def _test_cycle_keypair
      t_keyring = Crypto::KeyRing.new
      t_keypack = t_keyring.create('password1',auth,1024)

      t_keyring = Crypto::KeyRing.new

      assert_raises(Crypto::Error) {
         t_keyring.cycle_keypair(auth,1024)
      }

      t_keyring.authenticate('password1',auth,t_keypack)

      t_before = t_keyring.pubkey.serialize
      t_keyring.cycle_keypair(auth,1024)
      t_after = t_keyring.pubkey.serialize

      assert_not_equal t_before, t_after

   end
end



