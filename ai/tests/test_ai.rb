#!/bin/env ruby
#!/bin/env jruby
$VERBOSE = true

require 'pathname'
#require 'securerandom'

APP_ROOT ||= Pathname.new(ENV['APP_ROOT'])
#$LOAD_PATH.unshift("#{APP_ROOT}/common/lib")
#$LOAD_PATH.unshift("#{APP_ROOT}/client/lib")


require 'net/http'
require 'json'
require 'erb'

require 'test/unit'

class Test_PDC < Test::Unit::TestCase
   def setup
   end
   def teardown
   end
   def test_post
      t_uri = URI('http://localhost:4567/api/v1/pool/insert')
      t_http = Net::HTTP.new(t_uri.host,t_uri.port)
      t_req = Net::HTTP::Post.new(
         t_uri.path,
         'Content-Type' => 'application/json'
      )
      t_req.body = {to: 'user2',from: 'user1', payload: 'payload 1'}.to_json
      assert_kind_of Net::HTTPOK, t_http.request(t_req)
   end
   def test_get
      db_clear
      post_this('user2','user1','test1')
      post_this('user2','user1','test2')
      t_result = get('user2','2000-01-01 00:00:00.000000')
      assert_equal 2, t_result.count
      assert_equal ['test1','test2'], t_result.map { |r| r['payload'] }.sort
   end
   def get(i_to,i_timestamp)
      i_to = ERB::Util.url_encode(i_to)
      i_timestamp = ERB::Util.url_encode(i_timestamp)

      t_url = "http://localhost:4567/api/v1/pool/get/#{i_to}/#{i_timestamp}"
      t_uri = URI(t_url)
      t_response  = Net::HTTP.get(t_uri)
     JSON.parse(t_response)
   end
   def post_this(i_to,i_from,i_payload)
      t_uri = URI('http://localhost:4567/api/v1/pool/insert')
      t_http = Net::HTTP.new(t_uri.host,t_uri.port)
      t_req = Net::HTTP::Post.new(
         t_uri.path,
         'Content-Type' => 'application/json'
      )
      t_req.body = {to: i_to, from: i_from, payload:i_payload}.to_json
      t_http.request(t_req)
   end
   def db_clear
      t_url = "http://localhost:4567/api/v1/pool/clear"
      t_uri = URI(t_url)
      Net::HTTP.get(t_uri)
   end
end


