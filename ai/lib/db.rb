
require 'json'

module MyDarkWeb
module AI

class DB
   def initialize
      @data = []
   end
   def select(i_to,i_timestamp)
      #puts "#{i_to.inspect}"
      #puts "TIMESTAMP: #{i_timestamp.inspect}"
      @data.
         select { |r| r.to == i_to }.
         select { |r| r.timestamp > i_timestamp }.
         map { |r| r.to_h }.to_json
   end
   def insert(i_to,i_from,i_payload)
      @data.push << Record.new(i_to,i_from,i_payload)
      #@data.each { |r| puts '-'*79; puts r.inspect }
      #puts '-'*79
      self
   end
   def clear
      @data = []
   end
end

class Record
   attr_accessor :to, :from, :timestamp, :payload
   def initialize(i_to,i_from,i_payload)
      self.to = i_to
      self.from = i_from
      self.timestamp = Time.now.utc.strftime('%Y-%m-%d %H:%M:%S.%6N')
      self.payload = i_payload
   end
   def to_h
      {
         to: to,
         from: from,
         timestamp: timestamp,
         payload: payload,
      }
   end
end

end # module AI
end # module MyDarkWeb

