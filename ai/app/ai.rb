#!/usr/bin/env ruby
$VERBOSE = true

#==============================================================================
require 'pathname'
if ENV.has_key?('APP_ROOT')
   APP_ROOT = Pathname.new(ENV.fetch('APP_ROOT')) 
end
if ! self.class.const_defined?('APP_ROOT')
   APP_ROOT=Pathname.new($0).realpath.dirname.dirname.dirname
end
APP_ROOT.exist? or raise "path APP_ROOT=#{APP_ROOT} does not exist"

#==============================================================================
$LOAD_PATH.unshift("#{APP_ROOT}/ai/lib")

#==============================================================================
require 'sinatra'
require 'sinatra/namespace'
require 'sinatra/reloader'
require 'json'

#==============================================================================
require 'db'

DB = MyDarkWeb::AI::DB.new

namespace '/api/v1/pool' do
   get '/' do
      'Anonymous Interchange'
   end

   get '/get/:to/:timestamp' do
      DB.select(params[:to],params[:timestamp])
   end
   post '/insert' do
      t_data = JSON.parse(request.body.read,symbolize_names:true)
      DB.insert(
         t_data.fetch(:to),
         t_data.fetch(:from),
         t_data.fetch(:payload)
      )
      200
   end
   get '/clear' do
      DB.clear
   end
end

