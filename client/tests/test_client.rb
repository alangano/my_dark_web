#!/bin/env ruby
#!/bin/env jruby
$VERBOSE = true

#==============================================================================
require 'pathname'
if ENV.has_key?('APP_ROOT')
   APP_ROOT = Pathname.new(ENV.fetch('APP_ROOT')) 
end
if ! self.class.const_defined?('APP_ROOT')
   APP_ROOT=Pathname.new($0).realpath.dirname.dirname.dirname
end
APP_ROOT.exist? or raise "path APP_ROOT=#{APP_ROOT} does not exist"

#==============================================================================
$LOAD_PATH.unshift("#{APP_ROOT}/ai/lib")
$LOAD_PATH.unshift("#{APP_ROOT}/common/lib")
$LOAD_PATH.unshift("#{APP_ROOT}/client/lib")

require 'net/http'

require 'client'
require 'common/data_logger/logger'
#logger.handlers.clear


require 'test/unit'

class Test_PDC < Test::Unit::TestCase
   Client = MyDarkWeb::Client
   def setup
      drop_tree(APP_ROOT / "client/var/accounts")
   end
   def teardown
   end
   def drop_tree(i_dir)
      i_dir.exist? or return
      i_dir.children.each { |f|
         if f.directory?
            drop_tree(f)
            f.rmdir
         else
            f.unlink
         end
      }
   end
   def test_account_creation
      t_client = Client::Client.new('user1')
      assert_false t_client.exist?
      t_client.create('password')
      assert_true t_client.exist?
      t_client.open('password')
   end
   def test_loading_config
      t_client1 = Client::Client.new('user1')
      t_client1.create('password')
      t_client1.open('password')

      assert_nothing_raised { t_client1.config.ai_host }
      assert_nothing_raised { t_client1.config.ai_port }

   end
   def test_pubkey_handling
      t_client1 = Client::Client.new('user1')
      t_client1.create('password')
      t_client1.open('password')

      t_client2 = Client::Client.new('user2')
      t_client2.create('password')
      t_client2.open('password')

      # serialize for side-channel passing
      t_client2_pubkey = t_client2.keyring.pubkey.serialize
      t_client1_pubkey = t_client1.keyring.pubkey.serialize

      # key exchange
      t_client1.add_pubkey('user2',t_client2_pubkey)
      t_client2.add_pubkey('user1',t_client1_pubkey)
   end
   def test_message_passing
      db_clear

      #----------------------
      # setup
      #----------------------
      t_client1 = Client::Client.new('user1')
      t_client1.create('password')
      t_client1.open('password')

      t_client2 = Client::Client.new('user2')
      t_client2.create('password')
      t_client2.open('password')

      # key exchange
      t_client1.add_pubkey('user2',t_client2.keyring.pubkey.serialize)
      t_client2.add_pubkey('user1',t_client1.keyring.pubkey.serialize)

      #----------------------
      # client1 sends msg to client2
      #----------------------
      t_client1.send_message('user2','This is the message payload')

      #----------------------
      # Client 2 reads message
      #----------------------
      t_client2.download_messages
      assert_equal(
         'This is the message payload',
         t_client2.next_message['payload']
      )

      # one message sent and read - further downloads results in nil response
      # i.e., duplicate reads suppressed
      t_client2.download_messages
      assert_nil t_client2.next_message
   end
   def db_clear
      t_url = "http://localhost:4567/api/v1/pool/clear"
      t_uri = URI(t_url)
      Net::HTTP.get(t_uri)
   end
end


