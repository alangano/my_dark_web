
module MyDarkWeb
module Client
module KeyRing

class ForeignPubKeys
   attr_accessor :owner
   def initialize(i_owner)
      self.owner = i_owner
   end
   def insert(i_name,i_pubkey_ser)
      t_pubkey_flat = JSON.generate(JSON.parse(i_pubkey_ser))
      t_fpk = ForeignPubKey.new.set(owner,i_name,t_pubkey_flat)
      file.open('a+') { |fh|
         fh.puts t_fpk.to_json
      }
   end
   def find_by_pubkey(i_pubkey_b64)
      file.open('r') { |fh|
         fh.each_line.
            map { |raw| JSON.parse(raw,symbolize_names:true) }.
            #tap { |json| puts json.inspect }.
            map { |h| ForeignPubKey.new.load_from_hash(h) }.
            #tap { |list| list.each { |j| puts j.inspect } }.
            select { |r| r.owner == owner && r.pubkey.to_b64 == i_pubkey_b64 }.
            last
      }
   end
   def find_by_name(i_name)
      file.open('r') { |fh|
         fh.each_line.
            map { |raw| JSON.parse(raw,symbolize_names:true) }.
            #tap { |json| puts json.inspect }.
            map { |h| ForeignPubKey.new.load_from_hash(h) }.
            #tap { |list| list.each { |j| puts j.inspect } }.
            select { |r| r.owner == owner && r.username == i_name }.
            last
      }
   end
   def create
      file.open('w') { |fh| }
   end
   def file
      APP_ROOT / "client/var/accounts/#{owner}/keyring/foreign_pubkeys.dat"
   end
end

class ForeignPubKey
   attr_accessor :owner, :username, :pubkey_ser, :timestamp
   def initialize
      self.owner = nil
      self.username = nil
      self.pubkey_ser = nil
      self.timestamp = nil
   end
   def pubkey
      MyDarkWeb::Common::Crypto::PubKey.new.deserialize(pubkey_ser)
   end
   def load_from_hash(i_hash)
      self.owner = i_hash[:owner]
      self.username = i_hash[:name]
      self.pubkey_ser = i_hash[:pubkey_ser]
      self.timestamp = i_hash[:timestamp]
      self
   end
   def set(i_owner,i_username,i_pubkey_ser)
      self.owner = i_owner
      self.username = i_username
      self.pubkey_ser = i_pubkey_ser
      self.timestamp = Time.now.utc.strftime('%Y-%m-%d %H:%M:%S.%6N')
      self
   end
   def to_json
      JSON.generate(
         owner: owner,
         name: username,
         pubkey_ser: pubkey_ser,
         timestamp:  self.timestamp
      )
   end
#   def pubkey_id
#      JSON.parse(file.read).last
#   end
#   def add_pubkey(i_pubkey_id)
#      t_data = JSON.parse(file.read)
#      t_data << i_pubkey_id
#      file.open('w') { |f| f.puts JSON.generate(t_data) }
#      self
#   end
#   def file
#      APP_ROOT / "client/var/accounts/#{owner}/contacts/#{username}.json"
#   end
end

end # module KeyRing
end # module Client
end # module MyDarkWeb

