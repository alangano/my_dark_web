
require 'net/http'
require 'json'
require 'erb'

require_relative 'config'
require_relative 'keyring'

module MyDarkWeb
module Client

class Client
   attr_accessor :config
   attr_accessor :username
   attr_accessor :keyring
   attr_accessor :messages
   def initialize(i_username)
      logger.info "client starting"
      self.username = i_username

      logger.info 'loading config'
      self.config = Config.new

      self.keyring = KeyRing::KeyRing.new(username)

      self.messages = []
   end
   def download_messages
      logger.info 'downloading messages'
      t_to = ERB::Util.url_encode(keyring.keypair.pubkey.hex)

      last_timestamp_file.exist? or
         last_timestamp_file.open('w') { |fh|
            fh.puts '2000-01-01 00:00:00.000000'
         }

      t_timestamp = last_timestamp_file.read
      logger.info "reading from #{t_timestamp}"
      t_timestamp = ERB::Util.url_encode(t_timestamp)

      t_url = "#{uri_base}/get/#{t_to}/#{t_timestamp}"
      t_uri = URI(t_url)
      t_response  = JSON.parse(Net::HTTP.get(t_uri))
      t_response.
         sort_by { |r| r['timestamp'] }.
         #tap { |list| puts list.inspect }.
         each { |r|
            messages << r
            last_timestamp_file.open('w') { |fh|
               fh.puts r['timestamp']
            }
         }
      logger.info "new last-timestamp: #{last_timestamp_file.read}"
      self
   end
   def next_message
      t_msg = messages.shift
      t_msg or return nil

      t_fpk_b64 = Base64.strict_encode64([t_msg['from']].pack('H*'))

      t_fpk = keyring.foreign_pubkeys.find_by_pubkey(t_fpk_b64)
      t_fpk or raise "user by pubkey '#{t_msg['from']}' not found"

      t_msg['payload'] = keyring.keypair.decrypt(t_msg['payload'],t_fpk.pubkey)
      t_msg['foreign_pubkey'] = t_fpk
      t_msg
   end
   def send_message(i_to_name,i_text)
      logger.info "sending message to #{i_to_name}"
      t_fpk = keyring.foreign_pubkeys.find_by_name(i_to_name)
      t_fpk or raise "foreign name '#{i_to_name}', does not exist"
      t_payload = t_fpk.pubkey.encrypt(i_text,keyring.keypair)

      #puts "To: #{t_fpk.pubkey.hex}"
      #puts "From: #{keyring.pubkey.hex}"
      #puts "#{t_payload}"

      t_uri = URI("#{uri_base}/insert")
      t_http = Net::HTTP.new(t_uri.host,t_uri.port)
      t_req = Net::HTTP::Post.new(
         t_uri.path,
         'Content-Type' => 'application/json'
      )
      t_req.body = {
         to: t_fpk.pubkey.hex,
         from: keyring.pubkey.hex,
         payload: t_payload
      }.to_json
      r = t_http.request(t_req)
      r.kind_of?(Net::HTTPOK) or raise r.class
      self
   end
   def uri_base
      "http://#{config.ai_host}:#{config.ai_port}/api/v1/pool"
   end
   def add_pubkey(i_src_username,i_pubkey_ser)
      logger.info "adding pubkey from #{i_src_username}"
      keyring.foreign_pubkeys.insert(i_src_username,i_pubkey_ser)
      self
   end
   def open(i_password)
      keyring.open(i_password)
   end
   def create(i_password)
      exist? and raise "account already exists"

      logger.info 'creating account'
      account_dir.mkdir

      keyring.create(i_password)
      self
   rescue
      account_dir.exist? and account_dir.rmdir
      raise
   end
   def exist?
      account_dir.exist?
   end
   def last_timestamp_file
      account_dir / 'last_timestamp'
   end
   def account_dir
      (APP_ROOT / "client/var").tap { |d| d.exist? or d.mkdir }
      (APP_ROOT / "client/var/accounts").tap { |d| d.exist? or d.mkdir }
      APP_ROOT / "client/var/accounts/#{username}"
   end
end

end # module Client
end # module MyDarkWeb


