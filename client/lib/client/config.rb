
module MyDarkWeb
module Client

class Config
   attr_accessor :config
   def initialize
      logger.info 'loading config'
      self.config = JSON.parse(config_file.read,symbolize_names:true)
   end
   def ai_host
      config.fetch(:ai_host)
   end
   def ai_port
      config.fetch(:ai_port)
   end
   def config_file
      etc_dir / 'mdw.conf'
   end
   def etc_dir
      APP_ROOT / "client/etc"                     
   end
end

end # module Client
end # module MyDarkWeb

