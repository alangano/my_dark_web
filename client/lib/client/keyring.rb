
require 'common/crypto'

require_relative 'foreign_pubkeys'

module MyDarkWeb
module Client
module KeyRing

class KeyRing
   Crypto = MyDarkWeb::Common::Crypto
   attr_reader :username, :pubkey
   attr_reader :keypair
   attr_reader :foreign_pubkeys
   def initialize(i_username)
      @username = i_username
      close
   end
   def open(i_password)
      exist? or raise "keyring does not exist"
      logger.info 'opening keyring'

      logger.info "opening passkey: #{last_passkey_file.basename}"
      t_passkey = Crypto::PassKey.new.deserialize(last_passkey_file.read)
      t_passkey.set_passphrase(i_password)

      logger.info "opening mkey: #{last_mkey_file.basename}"
      t_mkey = Crypto::AuthenticatedSymKey.new.deserialize(
         t_passkey.decrypt(last_mkey_file.read)
      )

      logger.info "opening keypair: #{last_keypair_file.basename}"
      t_keypair = Crypto::KeyPair.new.deserialize(
         t_mkey.decrypt(last_keypair_file.read)
      )

      @passkey = t_passkey
      @mkey = t_mkey
      @keypair = t_keypair
      @pubkey = @keypair.pubkey
      @foreign_pubkeys = ForeignPubKeys.new(username)

   rescue
      logger.error 'rolling back keyring open'
      close
      raise
   end
   def close
      @passkey = nil
      @mkey = nil
      @keypair = nil
      @pubkey = nil
      @foreign_pubkeys = nil
   end
   def create(i_password)
      exist? and raise "keyring already exists"
      logger.info 'creating keyring'
      dir.mkdir

      t_passkey = Crypto::PassKey.new.create
      t_passkey.set_passphrase(i_password)
      t_passkey_ser = t_passkey.serialize
      new_passkey_file.open('w') { |f| f.puts t_passkey_ser }

      logger.info 'creating master symkey'
      t_mkey = Crypto::AuthenticatedSymKey.new.create
      new_mkey_file.open('w') { |f| f.puts t_passkey.encrypt(t_mkey.serialize) }

      logger.info 'creating keypair'
      t_keypair = Crypto::KeyPair.new.create
      new_keypair_file.open('w') { |f|
         f.puts t_mkey.encrypt(t_keypair.serialize)
      }

      logger.info 'creating foreign pubkey space'
      #(dir / 'pubkeys').mkdir
      t_foreign_pubkeys = ForeignPubKeys.new(username)
      t_foreign_pubkeys.create

      self

   rescue
      logger.error 'rolling back keyring creation'
      erase
      raise
   end
   def last_keypair_file
      dir.children.
         select { |f| f.basename.to_s =~ /^keypair-\d{8}-\d{6}\.dat$/ }.
         sort.
         last
   end
   def last_mkey_file
      dir.children.
         select { |f| f.basename.to_s =~ /^mkey-\d{8}-\d{6}\.dat$/ }.
         sort.
         last
   end
   def last_passkey_file
      dir.children.
         select { |f| f.basename.to_s =~ /^passkey-\d{8}-\d{6}\.dat$/ }.
         sort.
         last
   end
   def foreign_pubkey_file(i_hexcode)
      dir / "pubkeys/#{i_hexcode}.dat"
   end
   def new_mkey_file
      dir / "mkey-#{Time.now.strftime('%Y%m%d-%H%M%S')}.dat"
   end
   def new_keypair_file
      dir / "keypair-#{Time.now.strftime('%Y%m%d-%H%M%S')}.dat"
   end
   def new_passkey_file
      dir / "passkey-#{Time.now.strftime('%Y%m%d-%H%M%S')}.dat"
   end
   def exist?
      dir.exist?
   end
   def pubkeys_dir
      dir / 'pubkeys'
   end
   def dir
      APP_ROOT / "client/var/accounts/#{username}/keyring"
   end
   def erase(i_file=dir)
      puts i_file
      if i_file.directory?
         i_file.children.each { |f| erase(f) }
         i_file.rmdir
      else
         i_file.unlink
      end
   end
end

end # module KeyRing
end # module Client
end # module MyDarkWeb

