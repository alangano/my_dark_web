#!/usr/bin/env ruby
$VERBOSE = true

require 'pathname'
if ENV.has_key?('APP_ROOT')
   APP_ROOT = Pathname.new(ENV.fetch('APP_ROOT'))
end
if ! self.class.const_defined?('APP_ROOT')
   APP_ROOT=Pathname.new($0).realpath.dirname.dirname.dirname
end
APP_ROOT.exist? or raise "path APP_ROOT=#{APP_ROOT} does not exist"

$LOAD_PATH.unshift("#{APP_ROOT}/common/lib")
$LOAD_PATH.unshift("#{APP_ROOT}/client/lib")
require 'common/data_logger/logger'
logger.info "APP_ROOT=#{APP_ROOT}"

require 'client'

class CLIMsg
   def initialize
      @client = MyDarkWeb::Client::Client.new('poc')
      if ENV.has_key?('TEST_PASSWORD')
         t_password = ENV.fetch('TEST_PASSWORD')
      else
         print "password: "
         t_password = STDIN.gets.chomp
      end
      @client.exist? or setup(t_password)
      @client.open(t_password)
   end
   def run
      loop {
         begin
            run_cycle
         rescue => e
            puts "#{e.class} - #{e.message}"
         end
      }
   end
   def run_cycle
      print "> "
      t_cmd = gets.chomp.sub(/^\s*/,'').sub(/\s*$/,'')
      if t_cmd == 'help'
         do_help
      elsif t_cmd == ''
         return
      elsif t_cmd == 'print_my_pubkey'
         print_pubkey
      elsif t_cmd == 'add_pubkey'
         add_pubkey
      elsif t_cmd == 'send_msg'
         print 'target name: '
         t_name = STDIN.gets.chomp
         print 'msg: '
         t_msg = STDIN.gets.chomp
         send_msg(t_name,t_msg)
      elsif t_cmd == 'read_msg'
         read_msg
      else
         puts "command not understood"
      end
   end
   def do_help
      puts <<~END

         COMMANDS
            help              - this message
            print_my_pubkey   - print your pubkey hex-string to pass to others
            add_pubkey        - add a foreign pubkey to your keyring
            send_msg          - send a message
            read_msg          - read the next message (if any exist)

      END
   end
   def read_msg
      t_msg = @client.next_message
      if t_msg.nil?
         @client.download_messages
         t_msg = @client.next_message
      end
      if t_msg
         puts "FROM: #{t_msg['foreign_pubkey'].username}"
         puts "MSG: #{t_msg['payload']}"
      end
   end
   def send_msg(i_name,i_msg)
      @client.send_message(i_name,i_msg)
   end
   def add_pubkey
      print 'foreign name(e.g.,"Alan"): '
      t_name = STDIN.gets.chomp
      print 'pubkey: '
      t_pubkey = STDIN.gets.chomp

      t_pubkey_ser = [t_pubkey].pack('h*')
      @client.add_pubkey(t_name,t_pubkey_ser)

   end
   def print_pubkey
      puts ''
      puts "The following line must be passed to the foreign client"
      puts @client.keyring.pubkey.serialize.unpack('h*')
      puts ''
   end
   private
   def setup(i_password)
      logger.info "setting up a new client account"
      @client.create(i_password)
   end
end

t_cli = CLIMsg.new
t_cli.run

